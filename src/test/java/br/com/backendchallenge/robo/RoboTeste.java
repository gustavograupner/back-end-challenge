package br.com.backendchallenge.robo;

import org.junit.Assert;
import org.junit.Test;

import br.com.backendchallenge.enums.Direcao;
import br.com.backendchallenge.interfaces.IRobo;

public class RoboTeste {

	@Test
	public void teste360Direita() {
		IRobo robo = new Robo();
		robo.virarParaDireita();
		robo.virarParaDireita();
		robo.virarParaDireita();
		robo.virarParaDireita();
		
		Assert.assertEquals(Direcao.NORTH, robo.getPosicao().getDirecao());
	}
	
	@Test
	public void teste360Esquerda() {
		IRobo robo = new Robo();
		robo.virarParaEsquerda();
		robo.virarParaEsquerda();
		robo.virarParaEsquerda();
		robo.virarParaEsquerda();
		
		Assert.assertEquals(Direcao.NORTH, robo.getPosicao().getDirecao());
	}
	
	@Test
	public void testeMover() {
		IRobo robo = new Robo();
		robo.moverParaFrente();
		robo.virarParaDireita();
		robo.moverParaFrente();
		robo.virarParaEsquerda();
		robo.moverParaFrente();
		
		Assert.assertEquals(1, robo.getPosicao().getX());
		Assert.assertEquals(2, robo.getPosicao().getY());
	}

}
