package br.com.backendchallenge.rest;

import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;

public class MarteRestTeste extends AbstractRestTeste {

	@Test
	public void testarNavegacao() {
		Assert.assertEquals(Status.OK.getStatusCode(), getServico("MMMMM").request().post(null).getStatus());
		Assert.assertEquals(Status.OK.getStatusCode(), getServico("MMMM").request().post(null).getStatus());
	}

	@Test
	public void testarNavegacaoPosicaoInvalida() {
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), getServico("LM").request().post(null).getStatus());
	}
	
	@Test
	public void testarNavegacaoComandoInvalido() {
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), getServico("MLMMMCX").request().post(null).getStatus());
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), getServico("@!#$%").request().post(null).getStatus());
	}
	
}
