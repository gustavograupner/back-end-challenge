package br.com.backendchallenge.rest;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public abstract class AbstractRestTeste {
	
	private final String url = "http://localhost:8080/rest/mars/";
	
	protected ResteasyWebTarget getServico(String comando) {
		ResteasyClient cliente = new ResteasyClientBuilder().build();
		return cliente.target(url + comando);
	}
	
}
