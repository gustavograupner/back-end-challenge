package br.com.backendchallenge.util;

import org.junit.Test;

import br.com.backendchallenge.exceptions.PosicaoInvalidaException;

public class PosicaoTeste {

	@Test(expected = PosicaoInvalidaException.class)
	public void testePosicaoX() {
		Posicao posicao = new Posicao();
		posicao.setX(10);
	}
	
	@Test(expected = PosicaoInvalidaException.class)
	public void testePosicaoY() {
		Posicao posicao = new Posicao();
		posicao.setY(10);
	}

}
