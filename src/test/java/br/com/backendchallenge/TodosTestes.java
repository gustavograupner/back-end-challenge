package br.com.backendchallenge;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.backendchallenge.rest.MarteRestTeste;
import br.com.backendchallenge.robo.RoboTeste;
import br.com.backendchallenge.util.PosicaoTeste;

@RunWith(Suite.class)
@SuiteClasses({RoboTeste.class, PosicaoTeste.class, MarteRestTeste.class})
public class TodosTestes {}
