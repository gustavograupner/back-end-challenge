package br.com.backendchallenge.interfaces;

import br.com.backendchallenge.util.Posicao;

/**
 * Interface respons�vel por representar rob�
 * 
 * @author Gustavo
 *
 */
public interface IRobo extends IMovimentacao {
	Posicao getPosicao();	
}
