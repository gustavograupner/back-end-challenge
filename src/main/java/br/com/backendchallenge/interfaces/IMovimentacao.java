package br.com.backendchallenge.interfaces;

/**
 * Interface respons�vel por definir as movimenta��es do ve�culo
 * 
 * @author Gustavo
 *
 */
public interface IMovimentacao {
	void virarParaEsquerda();
	void virarParaDireita();
	void moverParaFrente();
}
