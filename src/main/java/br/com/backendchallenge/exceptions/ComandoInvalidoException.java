package br.com.backendchallenge.exceptions;

public class ComandoInvalidoException extends RuntimeException {
	
	public ComandoInvalidoException() {
		super();
	}
	
	public ComandoInvalidoException(String mensagem, Throwable throwable) {
		super(mensagem, throwable);
	}
}
