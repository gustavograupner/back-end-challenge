package br.com.backendchallenge.exceptions;

public class PosicaoInvalidaException extends RuntimeException {
	
	public PosicaoInvalidaException() {
		super();
	}
	
	public PosicaoInvalidaException(String mensagem, Throwable throwable) {
		super(mensagem, throwable);
	}
}
