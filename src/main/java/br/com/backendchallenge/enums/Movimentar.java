package br.com.backendchallenge.enums;

public enum Movimentar {
	LEFT("L"),
	RIGHT("R"),
	MOVE("M");
	
	private String direcao;
	
	private Movimentar(String direcao) {
		this.direcao = direcao;
	}
	
	public String getDirecao() {
		return direcao;
	}
}
