package br.com.backendchallenge.enums;

public enum Direcao {
	NORTH('N'),
	WEST('W'),
	EAST('E'),
	SOUTH('S');
	
	private char direcao;
	
	private Direcao(char direcao) {
		this.direcao = direcao;
	}
	
	public char getValor() {
		return direcao;
	}	
}
