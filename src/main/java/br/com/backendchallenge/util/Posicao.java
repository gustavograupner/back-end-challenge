package br.com.backendchallenge.util;

import br.com.backendchallenge.enums.Direcao;
import br.com.backendchallenge.exceptions.PosicaoInvalidaException;

/**
 * Classe que representa a posi��o do robo no mapa
 * 
 * @author Gustavo
 *
 */
public class Posicao {
	private int x;
	private int y;
	private Direcao direcao;

	{direcao = Direcao.NORTH;}	
	
	/**
	 * Define a posi��o X do rob�, caso valor esteja fora dos limites do mapa, gera a exce��o
	 * PosicaoInvalidaException
	 * 
	 * @param x
	 */
	public void setX(int x) {
		if (x > Mapa.LIMITE_MAX_X || x < Mapa.LIMITE_MIN_X) {
			throw new PosicaoInvalidaException();
		}
		this.x = x;
	}
	
	/**
	 * Define a posi��o Y do rob�, caso valor esteja fora dos limites do mapa, gera a exce��o
	 * PosicaoInvalidaException
	 * 
	 * @param x
	 */
	public void setY(int y) {
		if (y > Mapa.LIMITE_MAX_Y || y < Mapa.LIMITE_MIN_Y) {
			throw new PosicaoInvalidaException();
		}
		this.y = y;
	}	
	
	/**
	 * Define qual a dire��o que o rob� est� voltado.
	 * N - Norte	 
	 * W - Oeste
	 * S - Sul
	 * E - Leste
	 * 
	 * @param direcao
	 */
	public void setDirecao(Direcao direcao) {
		this.direcao = direcao;
	}
	
	public Direcao getDirecao() {
		return direcao;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public String toString() {
		return String.format("(%s,%s,%s)", 
				x, 
				y, 
				direcao.getValor());
	}
}
