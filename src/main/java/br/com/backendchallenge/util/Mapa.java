package br.com.backendchallenge.util;

/**
 * Classe que define as dimens�es do mapa de navega��o
 * 
 * @author Gustavo
 *
 */
public class Mapa {
	public static final int LIMITE_MAX_X = 5;
	public static final int LIMITE_MAX_Y = 5;
	public static final int LIMITE_MIN_X = 0;
	public static final int LIMITE_MIN_Y = 0;
}
