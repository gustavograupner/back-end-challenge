package br.com.backendchallenge.rest;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.backendchallenge.exceptions.ComandoInvalidoException;
import br.com.backendchallenge.exceptions.PosicaoInvalidaException;
import br.com.backendchallenge.interfaces.IRobo;
import br.com.backendchallenge.robo.Robo;

/**
 * Classe responsável por receber os comandos da base
 * 
 * @author Gustavo
 *
 */
@Path("/mars")
public class MarteRest {
	
	/**
	 * @param comandosNavegacao
	 * @return
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{comandosNavegacao}")	
	public Response executarComandoRobo(@PathParam("comandosNavegacao") String comandosNavegacao) {
		try {
			if (!comandosNavegacao.matches("[M,L,R]*")) {
				throw new ComandoInvalidoException();
			}
			
			String[] comandos = comandosNavegacao.split("");
			
			IRobo robo = new Robo();
			for (String comando : comandos) {
				switch (comando) {	
					case "L": { 
						robo.virarParaEsquerda();
						break;
					}
					case "R": {
						robo.virarParaDireita();
						break;
					}
					case "M": {
						robo.moverParaFrente();
						break;
					}
					default:{
						throw new ComandoInvalidoException();
					}
				}
			}
			return Response.status(Status.OK).entity(robo.getPosicao()).build();
		} catch (ComandoInvalidoException | PosicaoInvalidaException ex) {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
	
}
