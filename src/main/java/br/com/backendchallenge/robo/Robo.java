package br.com.backendchallenge.robo;

import br.com.backendchallenge.enums.Direcao;
import br.com.backendchallenge.interfaces.IRobo;
import br.com.backendchallenge.util.Posicao;

/**
 * Classe para representar o rob�, nesta classe � definida toda a
 * movimenta��o.
 * 
 * @author Gustavo
 *
 */
public class Robo implements IRobo {

	private Posicao posicao;
	
	{posicao = new Posicao();}
	
	/**
	 * Vira o rob� para a esquerda atualizando a posi��o 
	 */
	public void virarParaEsquerda() {
		switch (posicao.getDirecao()) {
			case NORTH : {
				posicao.setDirecao(Direcao.WEST);
				break;
			}
			case WEST : {
				posicao.setDirecao(Direcao.SOUTH);
				break;
			}
			case SOUTH : {
				posicao.setDirecao(Direcao.EAST);
				break;
			}
			case EAST : {
				posicao.setDirecao(Direcao.NORTH);
				break;
			}
		}
	}
	
	/**
	 * Vira o rob� para a direita atualizando a posi��o
	 */
	public void virarParaDireita() {
		switch (posicao.getDirecao()) {
			case NORTH : {
				posicao.setDirecao(Direcao.EAST);
				break;
			}
			case EAST : {
				posicao.setDirecao(Direcao.SOUTH);
				break;
			}
			case SOUTH : {
				posicao.setDirecao(Direcao.WEST);
				break;
			}
			case WEST : {
				posicao.setDirecao(Direcao.NORTH);
				break;
			}
		}
	}
	
	/**
	 * Move o rob� para frente 
	 *   
	 * Norte = x,y+1
	 * Sul 	 = x,y-1
	 * Leste = x+1,y	
	 * Oeste = x-1,y 
	 */
	public void moverParaFrente() {
		switch (posicao.getDirecao()) {
			case NORTH : {
				posicao.setX(posicao.getX());
				posicao.setY(posicao.getY() + 1);
				break;
			}
			case EAST : {
				posicao.setX(posicao.getX() + 1);
				posicao.setY(posicao.getY());
				break;
			}
			case SOUTH : {
				posicao.setX(posicao.getX());
				posicao.setY(posicao.getY() - 1);
				break;
			}
			case WEST : {
				posicao.setX(posicao.getX() - 1);
				posicao.setY(posicao.getY());
				break;
			}
		}	
	}
	
	/**
	 * Retorna a posi��o atual do rob�
	 */
	public Posicao getPosicao() {
		return posicao; 
	}
}
